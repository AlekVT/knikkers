﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Sessie
    {
        [Key]
        public int SessieID { get; set; }

        [Required]
        [Column(TypeName = "DateTime")]
        public DateTime Aangemaakt { get; set; }

        [Required]
        [Column(TypeName = "bit")]
        public bool Valid { get; set; }
    }
}