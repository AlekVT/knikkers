﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Knikkers
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Naam { get; set; }

        [Required]
        [Column(TypeName = "float")]
        public float Grootte { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(30)]
        public string Kleur { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(30)]
        public string Materiaal { get; set; }

        [Required]
        [Column(TypeName = "money")]
        public decimal Prijs { get; set; }
    }
}