﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class LoginDetails
    {
        [Key]
        public string UserName { get; set; }

        [Required]
		[DataType(DataType.Password)]
        [MinLength(6)]
        public string Password { get; set; }
    }
}