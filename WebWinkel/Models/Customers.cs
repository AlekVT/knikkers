﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Customers
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(25)]
        public string Voornaam { get; set; }

        [Required]
        [MaxLength(50)]
        public string Familienaam { get; set; }

        [Required]
        [MaxLength(200)]
        public string Straat { get; set; }

        [Required]
        [MaxLength(6)]
        [Column(TypeName = "varchar")]
        [DataType("int")]
        public string Huisnummer { get; set; }

        [Required]
        [MaxLength(50)]
        public string Land { get; set; }

        [MaxLength(75)]
        public string Email { get; set; }

        public virtual LoginDetails LoginData { get; set; }
        public virtual Postcodes Postcodes { get; set; }
    }
}