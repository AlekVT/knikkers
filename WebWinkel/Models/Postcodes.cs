﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebWinkel.Models
{
    public class Postcodes
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(4)]
        [Column(TypeName = "char")]
        [DataType("int")]
        public string Postcode { get; set; }

        [Required]
        [MaxLength(50)]
        public string Stad { get; set; }
    }
}