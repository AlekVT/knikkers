﻿using System;
using System.Web;

namespace WebWinkel.Helpers
{
    public class Cookies
    {
        private Session _sessie = new Session();

        private HttpCookie SetTime(HttpCookie cookie)
        {
            cookie.Expires = DateTime.Now.AddMinutes(2);
            return cookie;
        }

        private string[] DisectCookie(HttpCookie cookie)
        {
            string waarde = cookie.Value.ToString();
            return waarde.Split('|');
        }

        private int CookieID(string[] cookie)
        {
            int id = 0;
            int.TryParse(cookie[3], out id);
            return id;
        }

        public HttpCookie LoginCookie(string username)
        {
            var cookie = new HttpCookie("KnikkersStore", _sessie.Sessie(username));
            return SetTime(cookie);
        }

        public bool Verify(HttpCookie cookie)
        {
            if (cookie == null)
            {
                return false;
            }
            if (_sessie.Verify(DisectCookie(cookie)))
            {
                return true;
            }
            return false;
        }

        public HttpCookie Logout(HttpCookie cookie)
        {
            _sessie.Logout(CookieID(DisectCookie(cookie)));
            cookie.Value = false.ToString();
            return cookie;
        }
    }
}