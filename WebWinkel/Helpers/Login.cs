﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WebWinkel.Helpers
{
    public class Login
    {
        private static string _alias;
        private static string _password;
        private Models.KnikkerEntities db = new Models.KnikkerEntities();

        private string Encrypt(string data)
        {
            try
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    string hash = GetMd5Hash(Encode(md5Hash, data));
                    return hash;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        private byte[] Encode(MD5 md5Hash, string input)
        {
            return md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
        }

        private static string GetMd5Hash(byte[] data)
        {
            StringBuilder sB = new StringBuilder();

            foreach (byte b in data)
            {
                sB.Append(b.ToString("x2"));
            }

            return sB.ToString();
        }

        private Models.LoginDetails FindData()
        {
            try
            {
                return db.LoginDetails.Find(_alias);
            }
            catch (IndexOutOfRangeException)
            {
                throw;
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private Models.LoginDetails SetDetails()
        {
            Models.LoginDetails data = new Models.LoginDetails();

            data.UserName = _alias;
            data.Password = Encrypt(_password);

            return data;
        }

        private bool AddData(Models.LoginDetails data)
        {
            try
            {
                db.LoginDetails.Add(data);
                db.SaveChanges();
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }

        public Login(string alias, string password)
        {
            _alias = alias;
            _password = password;
        }

        public bool Verify()
        {
            Models.LoginDetails data = FindData();
            if (data == null)
            {
                return false;
            }
            if (data.Password == Encrypt(_password))
            {
                return true;
            }
            return false;
        }

        public bool Create()
        {
            if (db.LoginDetails.Find(_alias) != null || _alias == null)
            {
                return false;
            }

            return AddData(SetDetails());
        }
    }
}