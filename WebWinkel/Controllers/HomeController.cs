﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebWinkel.Controllers
{
    public class HomeController : Controller
    {
        private Models.KnikkerEntities db = new Models.KnikkerEntities();
        private Helpers.Cookies koeken = new Helpers.Cookies();

        private string Database()
        {
            if (new Helpers.DataBase(db).CreateDataBase())
            {
                return "Database loaded";
            }

            return "failed to load database";
        }

        public ActionResult Home()
        {
            ViewBag.DataBaseLoad = this.Database();
            ViewBag.LoggedIn = koeken.Verify(Request.Cookies["KnikkersStore"]);

            return View();
        }
    }
}