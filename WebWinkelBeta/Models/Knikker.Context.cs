﻿using System.Data.Entity;

namespace WebWinkel.Models
{
    public class KnikkerEntities: DbContext
    {
        public KnikkerEntities()
            :base ("Name=Jewels")
        {

        }

        public virtual DbSet<Knikkers> Knikkers { get; set; }
        public virtual DbSet<LoginDetails> LoginDetails { get; set; }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<OrderDetails> OrderDetails { get; set; }
        public virtual DbSet<Sessies> Sessies { get; set; }
        public virtual DbSet<Postcodes> Postcodes { get; set; }
    }
}