﻿using System.Web.Mvc;
using System.Net;

namespace WebWinkel.Controllers
{
    public class LoginController : Controller
    {
        private Models.KnikkerEntities db = new Models.KnikkerEntities();
        
        public ActionResult Login()
        {
            ViewBag.Test = "Nog niks gedaan";

            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "UserName,Password")] Models.LoginDetails data)
        {
            if (data == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!new Helpers.Login(data.UserName, data.Password).Verify())
            {
                ViewBag.Test = "false";
            }
            else
            {
                HttpContext.Response.SetCookie(new Helpers.Cookies().LoginCookie(data.UserName));
                ViewBag.Test = "true";
            }

            return View();
        }

        public ActionResult Register()
        {
            ViewBag.Test = "Nog niks gedaan";

            return View("Registration");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Helpers.HttpParamAction]
        public ActionResult Register([Bind(Include = "UserName,Password")] Models.LoginDetails data)
        {
            if (data == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!new Helpers.Login(data.UserName, data.Password).Create())
            {
                ViewBag.Test = "false";
            }
            else
            {
                ViewBag.Test = "true";
            }

            return View("Registration");
        }
    }
}