﻿using System;

namespace WebWinkel.Helpers
{
    public class DataBase
    {
        public Models.KnikkerEntities Entities { get; set; }

        public DataBase(Models.KnikkerEntities entities)
        {
            this.Entities = entities;
        }

        public bool CreateDataBase()
        {
            if (!Entities.Database.Exists())
            {
                try
                {
                    this.Entities.Database.Create();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            
            return true;
        }
    }
}