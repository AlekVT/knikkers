﻿using System;

namespace WebWinkel.Helpers
{
    public class Session
    {
        Models.KnikkerEntities db = new Models.KnikkerEntities();

        private Models.Sessies CreateSession(string id)
        {
            Models.Sessies sessie = new Models.Sessies();

            sessie.Aangemaakt = DateTime.Now;
            sessie.Valid = true;
            sessie.LoginDetails = db.LoginDetails.Find(id);

            if (AddSession(sessie))
            {
                return sessie;
            }
            else
            {
                return null;
            }
        }

        private bool AddSession(Models.Sessies sessie)
        {
            try
            {
                db.Sessies.Add(sessie);
                db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private string SessieString(Models.Sessies sessie)
        {
            return $"{sessie.SessieID}|{sessie.Aangemaakt.ToString()}|{sessie.Valid.ToString()}";
        }

        public string Sessie(string id)
        {
            Models.Sessies sessie = CreateSession(id);
            if (sessie.Valid == true)
            {
                return SessieString(sessie);
            }
            return null;
        }

        public bool Verify(string[] cookie)
        {
            if (cookie[0] != "false")
            {

                Models.Sessies sessie = db.Sessies.Find(int.Parse(cookie[0]));

                if (sessie.Aangemaakt.ToString().Equals(cookie[1]) && sessie.Valid.ToString().Equals(cookie[2]))
                {
                    return true;
                }
            }
            return false;
        }

        public void Logout(int id)
        {
            db.Sessies.Find(id).Valid = false;
        }
    }
}