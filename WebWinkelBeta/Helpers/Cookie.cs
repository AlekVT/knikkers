﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebWinkel.Helpers
{
    public class Cookie
    {
        HttpCookieCollection cookies = new HttpCookieCollection();

        public HttpCookie Create()
        {
            HttpCookie cookie = new HttpCookie("KnikkersStore", $"{DateTime.Now}|{true}");
            cookie.Expires = DateTime.Now.AddMinutes(15);

            return cookie;
        }

        public bool RefreshCookie()
        {
            cookies.Get("KnikkerStore").Expires = DateTime.Now.AddMinutes(15);
            return true;
        }

        public bool Logout ()
        {
            try
            {
                cookies.Get("KnikkerStore").Value = $"{false}";
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public bool CheckCookie()
        {
            if (cookies.Get("KnikkerStore").HasKeys)
            {
                return true;
            }
            return false;
        }
    }
}