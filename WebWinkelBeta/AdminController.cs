﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebWinkel.Models;

namespace WebShop.Controllers
{
	public class AdminController : Controller
	{
		private KnikkerEntities db = new KnikkerEntities();

		
		// GET: Orders
		public ActionResult Admin()
		{
			var orders = db.Orders.Include(o => o.KlantID)
				.Include(o => o.BestelDatum)
				.Include(o => o.TotaalPrijs)
				.Include(o => o.Retour)
				.Include(o => o.Betaald)
				.Include(o => o.Aantal);

			return View(orders.ToList());
		}

		//[Authorize(Roles = "Admin")]
		//// GET: Orders/Details/5
		//public ActionResult Details(int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	Orders orders = db.Orders.Find(id);
		//	if (orders == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(orders);
		//}

		//[Authorize(Roles = "Admin")]
		//// GET: Orders/Create
		//public ActionResult Create()
		//{
		//	ViewBag.CustomerID = new SelectList(db.Customers, "ID");
		//	return View();
		//}

		//[Authorize(Roles = "Admin")]
		//// POST: Orders/Create
		//// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		//// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Create([Bind(Include = "OrderID,CustomerID,EmployeeID,OrderDate,RequiredDate,ShippedDate,ShipVia,Freight,ShipName,ShipAddress,ShipCity,ShipRegion,ShipPostalCode,ShipCountry")] Orders orders)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		db.Orders.Add(orders);
		//		db.SaveChanges();
		//		return RedirectToAction("Index");
		//	}
		//	ViewBag.CustomerID = new SelectList(db.Customers, "ID", "ID");
		//	return View(orders);
		//}

		//[Authorize(Roles = "Admin")]
		//// GET: Orders/Edit/5
		//public ActionResult Edit(int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	Orders orders = db.Orders.Find(id);
		//	if (orders == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	ViewBag.CustomerID = new SelectList(db.Customers, "CustomerID", "CompanyName", orders.CustomerID);
		//	ViewBag.EmployeeID = new SelectList(db.Employees, "EmployeeID", "LastName", orders.EmployeeID);
		//	ViewBag.ShipVia = new SelectList(db.Shippers, "ShipperID", "CompanyName", orders.ShipVia);
		//	return View(orders);
		//}

		//[Authorize(Roles = "Admin")]
		//// POST: Orders/Edit/5
		//// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		//// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		//[HttpPost]
		//[ValidateAntiForgeryToken]
		//public ActionResult Edit([Bind(Include = "OrderID,CustomerID,EmployeeID,OrderDate,RequiredDate,ShippedDate,ShipVia,Freight,ShipName,ShipAddress,ShipCity,ShipRegion,ShipPostalCode,ShipCountry")] Orders orders)
		//{
		//	if (ModelState.IsValid)
		//	{
		//		db.Entry(orders).State = EntityState.Modified;
		//		db.SaveChanges();
		//		return RedirectToAction("Index");
		//	}
		//	ViewBag.CustomerID = new SelectList(db.Customers, "CustomerID", "CompanyName", orders.CustomerID);
		//	ViewBag.EmployeeID = new SelectList(db.Employees, "EmployeeID", "LastName", orders.EmployeeID);
		//	ViewBag.ShipVia = new SelectList(db.Shippers, "ShipperID", "CompanyName", orders.ShipVia);
		//	return View(orders);
		//}

		//[Authorize(Roles = "Admin")]
		//// GET: Orders/Delete/5
		//public ActionResult Delete(int? id)
		//{
		//	if (id == null)
		//	{
		//		return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
		//	}
		//	Orders orders = db.Orders.Find(id);
		//	if (orders == null)
		//	{
		//		return HttpNotFound();
		//	}
		//	return View(orders);
		//}

		//[Authorize(Roles = "Admin")]
		//// POST: Orders/Delete/5
		//[HttpPost, ActionName("Delete")]
		//[ValidateAntiForgeryToken]
		//public ActionResult DeleteConfirmed(int id)
		//{
		//	Orders orders = db.Orders.Find(id);
		//	db.Orders.Remove(orders);
		//	db.SaveChanges();
		//	return RedirectToAction("Admin");
		//}

		//[Authorize(Roles = "Admin")]
		//protected override void Dispose(bool disposing)
		//{
		//	if (disposing)
		//	{
		//		db.Dispose();
		//	}
		//	base.Dispose(disposing);
		//}

		//[Authorize(Roles = "Admin")]
		//// GET: Orders/Details/5
		//[HttpGet]
		//public ActionResult ShippingOrder(string searchString)
		//{
		//	if ((!String.IsNullOrEmpty(searchString)) && (searchString != "Search id here..."))
		//	{
		//		var foor = db.Orders.Where(s => s.OrderID.ToString().Contains(searchString));
		//		return View(foor);
		//	}
		//	else
		//	{
		//		return View(db.Orders);
		//	}
		//}

		//[Authorize(Roles = "Admin")]
		//public ActionResult OutstandingOrders(string searchString)
		//{
		//	if ((!String.IsNullOrEmpty(searchString)) && (searchString != "Search id here..."))
		//	{
		//		var foor = db.Orders.Where(s => s.OrderID.ToString().Contains(searchString));
		//		return View(foor);
		//	}
		//	else
		//	{
		//		return View(db.Orders);
		//	}
		//}

		//[Authorize(Roles = "Admin")]
		//public ActionResult CompletedOrders(string searchString)
		//{
		//	if ((!String.IsNullOrEmpty(searchString)) && (searchString != "Search id here..."))
		//	{
		//		var foor = db.Orders.Where(s => s.OrderID.ToString().Contains(searchString));
		//		return View(foor);
		//	}
		//	else
		//	{
		//		return View(db.Orders);
		//	}
		//}
	}
}
